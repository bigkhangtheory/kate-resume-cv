// English
const lang = {
    contact: 'Contact',
    born: 'Born',
    bornIn: 'in',
    experience: 'Experience',
    education: 'Education',
    skills: 'Skills',
    about: 'About me',
    summary: 'Summary',
    certifications: 'Certifications'
};
export default lang;
